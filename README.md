# README #

Basic Debian 8 image with some developer tools added.  This has been set up as a base for a LAMP server image which is built on top but could be used as a base image for other images.

Further developer tools will be added over time.
